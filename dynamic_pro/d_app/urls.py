from django.urls import path
from . import views


urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('post/', views.PostView.as_view(), name='post'),
    path('author/', views.AuthorView.as_view(), name='author'),
    path('search/', views.SearchView.as_view(), name='search'),
    path('post/<slug:slug>/', views.IndexDetailsView.as_view(), name='detail'),
    path('author/<int:user_id>/', views.AuthorDetailView.as_view(), name='authordetail'),


]
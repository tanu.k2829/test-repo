from django.contrib import admin
from d_app.models import *


@admin.register(Blog)
class BlogModelAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'user_id', 'heading', 'slug']



from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
from django_extensions.db.fields import AutoSlugField
from d_app.utils import Timestamp


class Blog(Timestamp):
    image = models.ImageField(_('Image'), upload_to='uploads/')
    about = models.CharField(max_length=255, default=True)
    heading = models.CharField(_('Heading'), max_length=255)
    description = models.TextField(_('Description'))
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Blog', default=True)
    slug = AutoSlugField(populate_from=['heading'], null=True, blank=True)

    def __str__(self):
        return self.heading

    def get_absolute_url(self):
        return reverse("post", kwargs={"slug": self.slug, 'user_id': self.user.id})





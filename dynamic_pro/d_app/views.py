
from constance.admin import config
from django.db.models import Q
from django.http import request
from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView

from .models import *


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['some_blog'] = Blog.objects.order_by('-created_at')[:4]
        context['blog'] = Blog.objects.order_by('-created_at')[4:]
        return context


class PostView(TemplateView):
    template_name = 'post.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['post1'] = Blog.objects.all()
        context['author'] = Blog.objects.all
        return context


class AuthorView(ListView):
    template_name = 'author.html'

    def get_queryset(self):
        object = Blog.objects.all()
        return object

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["config"] = config
        return context


class SearchView(ListView):
    model = Blog
    template_name = "search.html"

    def get_queryset(self):
        query = self.request.GET.get('search')
        object_list = Blog.objects.filter(Q(heading__icontains=query) | Q(description__icontains=query))
        return object_list


class IndexDetailsView(DetailView):
    model = Blog
    template_name = 'post.html'


class AuthorDetailView(DetailView):
    template_name = 'author_detail.html'
    queryset = Blog.objects.all()

    def get(self, request, *args, **kwargs):
        blog = Blog.objects.filter(user_id=kwargs.get('user_id'))
        user = User.objects.filter(id=kwargs.get('user_id'))
        return render(request, self.template_name, {'blog': blog, 'user': user})

from django.db import models
from django.utils.translation import gettext as _
from datetime import datetime
from django.utils import timezone


class Timestamp(models.Model):
    created_at = models.DateTimeField(_('Created_At'), default=timezone.now())
    updated_at = models.DateTimeField(_('Updated_At'), default=timezone.now())

    class Meta:
        abstract = True

    def __str__(self):
        return "created_at {}, updated_at {}".format(self.created_at, self.updated_at)
